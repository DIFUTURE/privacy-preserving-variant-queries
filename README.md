# Scalable and Privacy-Preserving Whole Genome Variant Queries

This project provides a method that uses secure multi-party computation (SMC) to query genomic databases in a privacy-protected manner. The proposed solution allows genomic databases to be safely stored in semi-honest cloud environments. It provides data privacy, query privacy, and output privacy. It allows multiple genomic databases to be queried at the same time and t is possible to query multiple databases with response times fast enough for practical application. We use the [ABY](https://github.com/encryptogroup/ABY) library for secure two-party computation. 


### Building

1. Clone the git repository by running 

    ```
    git clone https://gitlab.com/DIFUTURE/privacy-preserving-variant-queries.git
    ```
    
2. Enter the directory

    ```
    cd privacy-preserving-variant-queries/
    ```
    
3. Run the following command to compile the code

    ```
    make
    ```
    
### Using the Code


* VCF2Shares.py
  * Converts a given single sample VCF file to two boolean shares. It generates two isomorphic trees 
  * Parameters: 
    * `-i IFILE     Input VCF File`
    * `-o OFILE     Output file`
    * `-s START     Starting line`
    * `-v NVARIANT  Number of Variants (0=all)`
    * `-l BITLEN    Bit Length of Variant`
  * Usage
    * `python VCF2Shares.py -i <VCF File> -o <Output File (prefix)> -s <Starting line> -v <Number of Variants> -l <Bit Length of Variant>`

* Variant2Shares.py
  * Converts a given variant to two boolean shares
  * Parameters: 
    * `-c CHR     CHROM`
    * `-p POS     POS`
    * `-r REF     REF`
    * `-a ALT     ALT`
  * Usage
    * `python Variant2Shares.py -c <CHROM> -p <POS> -r <REF> -a <ALT>`



* Our Solution
  * Our implementations can work on real VCF files and synthetic data. You can run the programs with the following set of commands:
  * Parameters: 
    *  `-r [Role: 0/1, required]`
    *  `-i [Input Files (separate multiple files with comma), optional]`
    *  `-q [Queries (separate multiple queries with comma), optional]`
    *  `-v [Number of Variants default: 100000, optional]`
    *  `-n [Number of Queries default: 5, optional]`
    *  `-l [The Bit Lenght of Variant default: 48, optional]`
    *  `-h [Number of Patient default: 1, optional]`
    *  `-a [IP-address, default: localhost, optional]`

  * Usage (real VCF files)
      * Server 1: `./bin/variant_queries.exe -r 0 -i <file1,file2,...,fileN> -q <query1,query2,...,queryM> -l 48 -a <ip of the server>`
      * Server 2: `./bin/variant_queries.exe -r 1 -i <file1,file2,...,fileN> -q <query1,query2,...,queryM> -l 48 -a <ip of the server>`
      * Example
        * Server 1: `./bin/variant_queries.exe -r 0 -i inputs/son.vcfx,inputs/mother.vcfx,inputs/father.vcfx -q 214514074238257,255612459705535 -l 48 -a 127.0.0.1`
        * Server 2: `./bin/variant_queries.exe -r 1 -i inputs/son.vcfy,inputs/mother.vcfy,inputs/father.vcfy -q 25797506136692,56523544918126 -l 48 -a 127.0.0.1`
  * Usage (synthetic data)
      * Server 1: `./bin/variant_queries.exe -r 0 -v 100000 -h 5 -l 48 -n 5 -a <ip of the server>`
      * Server 2: `./bin/variant_queries.exe -r 1 -v 100000 -h 5 -l 48 -n 5 -a <ip of the server>`
* Demmler et al.'s solution
  * Server 1: `./bin/cans2017.exe -r 0 -v 100000 -l 48 -q 5 -a <ip of the server>`
  * Server 2: `./bin/cans2017.exe -r 1 -v 100000 -l 48 -q 5 -a <ip of the server>`
  * Parameters: 
    *  `-r [Role: 0/1, required]`
    *  `-v [Number of Variants, required]`
    *  `-l [Bit Length of Variant, required]`
    *  `-q [Number of Queries, required]`
    *  `-a [IP-address, default: localhost, optional]`

### Sample Input Files

* son.vcfx and son.vcfy were generated with the following command:
  * `python VCF2Shares.py -i HG002_GRCh38_GIAB_highconf_CG-Illfb-IllsentieonHC-Ion-10XsentieonHC-SOLIDgatkHC_CHROM1-22_v.3.3.2_highconf_triophased.vcf -v 10000 -l 48`

* mother.vcfx and mother.vcfy were generated with the following command:
  * `python VCF2Shares.py -i HG004_GRCh38_GIAB_highconf_CG-Illfb-IllsentieonHC-Ion-10XsentieonHC_CHROM1-22_v.3.3.2_highconf.vcf -v 10000 -l 48`

* father.vcfx and father.vcfy were generated with the following command:
  * `python VCF2Shares.py -i HG003_GRCh38_GIAB_highconf_CG-Illfb-IllsentieonHC-Ion-10XsentieonHC_CHROM1-22_v.3.3.2_highconf.vcf -v 10000 -l 48`

VCF files above can be downloaded from [ftp://ftp-trace.ncbi.nlm.nih.gov/giab/ftp/release/AshkenazimTrio/](ftp://ftp-trace.ncbi.nlm.nih.gov/giab/ftp/release/AshkenazimTrio/)

### Sample Variant Queries
* 255612459705535 and 56523544918126 were generated with the following command: 
    * `python Variant2Shares.py -c chr1 -p 826577 -r A -a AT`
    * This variant was taken from `HG002_GRCh38_GIAB_highconf_CG-Illfb-IllsentieonHC-Ion-10XsentieonHC-SOLIDgatkHC_CHROM1-22_v.3.3.2_highconf_triophased.vcf`

* 214514074238257 and 25797506136692 were generated with the following command: 
    * `python Variant2Shares.py -c chr1 -p 116549 -r C -a T`
    * This variant was taken from `HG002_GRCh38_GIAB_highconf_CG-Illfb-IllsentieonHC-Ion-10XsentieonHC-SOLIDgatkHC_CHROM1-22_v.3.3.2_highconf_triophased.vcf`

