#!/usr/bin/python

import hashlib
import math
import random
import sys
import argparse
import copy

class Node:

    def __init__(self, data):

        self.left = None
        self.right = None
        self.y = random.randint(0, 0x1)
        self.x = data ^ self.y

    def insert(self, value, level):
        level += 1
        which_child = 0
            
        if level < results.bitlen:
            if self.left and self.left.x ^ self.left.y == (value>>level) & 0x1:
                self.left.insert(value, level)
            elif self.right and self.right.x ^ self.right.y == (value>>level) & 0x1:
                self.right.insert(value, level)
            else:
                if which_child == 0:
                    if self.left is None:
                        self.left = Node(value >> level & 0x1)
                        self.left.insert(value, level)
                    else:
                        self.right = Node(value >> level & 0x1)
                        self.right.insert(value, level)
                else:
                    if self.right is None:
                        self.right = Node(value >> level & 0x1)
                        self.right.insert(value, level)
                    else:
                        self.left = Node(value >> level & 0x1)
                        self.left.insert(value, level)
		

    def PreorderTraversalX(self, root):
        res = ''
        if root:
            if root.x == 0:
                res += '00'
            else:
                res += '11'
            res = res + self.PreorderTraversalX(root.left)
            res = res + self.PreorderTraversalX(root.right)
        else:
            res += '10'
        return res

    def PreorderTraversalY(self, root):
        res = ''
        if root:
            if root.y == 0:
                res += '00'
            else:
                res += '11'
            res = res + self.PreorderTraversalY(root.left)
            res = res + self.PreorderTraversalY(root.right)
        else:
            res += '10'
        return res


if __name__ == '__main__':
    parser = \
        argparse.ArgumentParser(description='Converts a given simple sample VCF File to two boolean shares'
                                )
    parser.add_argument('-i', action='store', dest='ifile',
                        required=True, help='Input VCF File')
    parser.add_argument('-o', action='store', dest='ofile',
                        required=True, help='Output VCF File')

    parser.add_argument(
        '-v',
        action='store',
        dest='nvariant',
        default=4000000000,
        type=int,
        help='Number of Variants (0=all)',
        )
    parser.add_argument(
        '-s',
        action='store',
        dest='start',
        default=0,
        type=int,
        help='Starting line',
        )
    parser.add_argument(
        '-l',
        action='store',
        dest='bitlen',
        required=True,
        type=int,
        help='Bit Length of Variant',
        )
    results = parser.parse_args()

       

    print(results.ifile,results.start,results.nvariant)
    f = open(results.ifile, 'r')

    count = 0
    chro = 0
    pos = 0
    root = Node(0x1)
    chr_size = {}
    genome_size = 0;
    for line in f:
        if line.startswith('##contig'):
            chr_size[line.split(',')[0].split('=')[2]]=genome_size
            genome_size += int(line.split('=')[3][:-2])
        if line.startswith('#CHROM'):
            break
    for line in f:
        if count >= results.start:
            cols = line.split('\t')
            upos = chr_size[cols[0]] + int(cols[1])

            m = hashlib.md5()
            m.update((cols[0] + cols[1] + cols[3] + cols[4]).encode())
            value = int(m.hexdigest()[:4], 16)

            pos_value = (value << 32) + upos
            root.insert(pos_value, -1)
        count += 1
        if count % 10000 == 0:
            print (count, ' variants are processed')
        if count == (results.nvariant+results.start):
            break

    f.close()
    f = open(results.ofile+"x", 'w')
    f.write(root.PreorderTraversalX(root))
    f.close()
    f = open(results.ofile+"y", 'w')
    f.write(root.PreorderTraversalY(root))
    f.close()
