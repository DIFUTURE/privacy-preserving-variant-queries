#ifndef __VARIANTQUERIES_H_
#define __VARIANTQUERIES_H_

#include "../../../abycore/circuit/booleancircuits.h"
#include "../../../abycore/circuit/arithmeticcircuits.h"
#include "../../../abycore/circuit/circuit.h"
#include "../../../abycore/aby/abyparty.h"
#include <math.h>
#include <cassert>
#include "tree.h"



int32_t test_variant_queries(e_role role, char* address, uint16_t port, seclvl seclvl,
                uint32_t bitlen,e_mt_gen_alg mt_alg,
                e_sharing sharing,uint32_t nvariant, uint32_t nquery, uint32_t npatient,string files, string queries);
uint8_t getVal(uint8_t *array, uint32_t ind);

#endif /* __VARIANTQUERIES_H_ */