#include "variantqueries.h"

int32_t test_variant_queries(e_role role, char* address, uint16_t port, seclvl seclvl, uint32_t bitlen, e_mt_gen_alg mt_alg, e_sharing sharing, uint32_t nvariant, uint32_t nquery, uint32_t npatient,string files,string queries) {

    uint16_t depth = bitlen;
    srand(time(NULL));
    uint32_t qdsz;
    uint64_t* qu;
    uint64_t* vr;
    node** r;
    int fcount=0;
    int qcount=0;
    tree* trees;
    if (files.compare("") != 0) {
        uint32_t commaCount = 1;
        for (int i=0; i<files.size(); i++) {
            if (files[i] == ',')
                commaCount++;
        }
        cout<<"Number of input files = "<<commaCount<<endl;
        trees = new tree(commaCount);
        files.append(1,',');
        string delimiter = ",";
        size_t pos = 0;
        string token;
        while ((pos = files.find(delimiter)) != string::npos) {
            token = files.substr(0, pos);
            string bits;
            ifstream f (token);
            if (f.is_open())
            {
                if (f.good() )
                {
                    getline(f,bits);
                }
                f.close();
                cout<<token<<" file are processing"<<endl;
                trees->setValues(bits,fcount);
                cout<<trees->getNodeCount()<<endl;
                fcount++;
            } else {
                cout<<"File:"<<token<<" can not be opened"<<endl;
                delete trees;
                return -1;
            }
            files.erase(0, pos + delimiter.length());
        }
        delimiter = ",";
        pos = 0;
        uint64_t qn[1000];
        if (queries.compare("") != 0) {
            queries.append(1,',');
            while ((pos = queries.find(delimiter)) != string::npos) {
                token = queries.substr(0, pos);
                qn[qcount]=strtoul(token.c_str(),NULL,0);
                qcount++;
                queries.erase(0, pos + delimiter.length());
            }
        }
        if (qcount == 0 || fcount==0) {
            cout<<"Missing files or queries"<<endl;
            delete trees;
            return -1;

        }
        qdsz = fcount*qcount;
        qu = new uint64_t[qdsz];
        vr = new uint64_t[qdsz];
        r = new node*[qdsz];
        for (int i=0; i<fcount; i++) {
            for (int j=0; j<qcount; j++) {
                r[(i*qcount)+j] = trees->getRoot();
                qu[(i*qcount)+j] = qn[j];
            }
        }
        npatient = fcount;
        nquery = qcount;

    } else {

        // for our tests, we create variant trees in random fashion
        // on one side the tree stores bits of path value
        // on another side the tree stores random values
        trees = new tree(depth,role,npatient);
        uint64_t step = 3000000000/nvariant;
        uint64_t loc = 0;
        uint64_t val = 0;
        for (int h =0; h<npatient; h++) {
            trees->setPatient(h);
            for (int i=0; i<nvariant; i++) {
                uint64_t rn = val;
                uint64_t path = (loc)^(rn<<32);
                trees->insert(path);
                val= (val+step)%65536;
                loc= (loc+step)%3000000000;
            }
            cout<<"Dummy data was generated for patient "<<h<<"."<<endl;
            cout<<trees->getNodeCount()<<endl;
        }
        qdsz = nquery*npatient;
        qu = new uint64_t[qdsz];
        vr = new uint64_t[qdsz];
        r = new node*[qdsz];
        loc = 0;
        val = 0;
        // for testing, we chosee variants like i=i^0
        for (int i=0; i<qdsz; i++) {
            if (role == SERVER)
                qu[i] = 0;
            else {
                uint64_t rn = val;
                uint64_t path = (loc)^(rn<<32);
                qu[i] = path;
                val= (val+step)%65536;
                loc= (loc+step)%3000000000;
            }
            r[i] = trees->getRoot();
        }
    }


    ABYParty* party = new ABYParty(role, address, port, seclvl, bitlen, 1, mt_alg);

    double o_time  = 0;
    double os_time  = 0, o_comm = 0, s_comm = 0;


    vector<Sharing*>& sharings = party->GetSharings();

    Circuit* circ = sharings[sharing]->GetCircuitBuildRoutine();


    share *s_query, *s_variant, *s_out;


    uint8_t* q_bits = new uint8_t[qdsz];
    uint8_t* v_bits = new uint8_t[qdsz];


    uint32_t output = 0;


    for (int i=0; i<depth; i++) {
        for (int j=0; j<qdsz; j++) {
            if (r[j]->left != NULL) {
                uint8_t tmp = getVal(r[j]->left->val, j/nquery);
                if (tmp != 2)
                    v_bits[j] = tmp;
                else {
                    v_bits[j] = getVal(r[j]->right->val, j/nquery);
                }

            }
            else {
                v_bits[j] = getVal(r[j]->right->val, j/nquery);
            }
        }
        s_variant = circ->PutSharedSIMDINGate(qdsz,v_bits, 1);
        for (int j=0; j<qdsz; j++) {
            q_bits[j] = ((qu[j]>>i)&0x1);
        }
        s_query = circ->PutSharedSIMDINGate(qdsz,q_bits,1);


        s_out = circ->PutEQGate(s_variant, s_query);

        s_out = circ->PutOUTGate(s_out, ALL);

        party->ExecCircuit();

        // calculating metrics of one round
        // calculation for one level of variant tree
        o_time += party->GetTiming(P_ONLINE);
        if (i==0) {
            os_time += party->GetTiming(P_SETUP);
            s_comm += party->GetSentData(P_SETUP);
        }
        o_comm += party->GetSentData(P_ONLINE);

        uint32_t out_bitlen,out_nvals,*out_vals;
        s_out->get_clear_value_vec(&out_vals,&out_bitlen,&out_nvals);



        // generating possible variant values from comprasion outputs. in real application
        // client sends them to proxy servers
        for (int j=0; j<qdsz; j++) {
            if (i==0)
                vr[j]=0;
            if (out_vals[j] == 1) {
                if (r[j]->left != NULL) {
                    uint8_t tmp = getVal(r[j]->left->val, j/nquery);
                    if (tmp!=2) {
                        vr[j] = vr[j]^(((uint64_t)tmp)<<i);
                        r[j] = r[j]->left;
                    }
                    else {
                        vr[j] = vr[j]^(((uint64_t)getVal(r[j]->right->val, j/nquery))<<i);
                        r[j] = r[j]->right;
                    }

                } else {
                    vr[j] = vr[j]^(((uint64_t)getVal(r[j]->right->val, j/nquery))<<i);
                    r[j] = r[j]->right;
                }
            }
            else {
                if (r[j]->right != NULL) {
                    uint8_t tmp = getVal(r[j]->right->val, j/nquery);
                    if (tmp!=2) {
                        vr[j] = vr[j]^(((uint64_t)tmp)<<i);
                        r[j] = r[j]->right;
                    } else {
                        vr[j] = vr[j]^(((uint64_t)getVal(r[j]->left->val, j/nquery))<<i);
                        r[j] = r[j]->left;
                    }
                } else {
                    vr[j] = vr[j]^(((uint64_t)getVal(r[j]->left->val, j/nquery))<<i);
                    r[j] = r[j]->left;
                }
            }
        }

        party->Reset();
    }


    delete[] q_bits;
    delete[] v_bits;

    // compare possbile variant values with query variants
    s_variant = circ->PutSharedSIMDINGate(qdsz,vr,bitlen);
    s_query = circ->PutSharedSIMDINGate(qdsz, qu,bitlen);
    s_out = circ->PutEQGate(s_query,s_variant);

    uint32_t constant = 0;
    uint32_t sum_sz = 1;
// calculate the bit length of sum variable
    while (1) {
        if (sum_sz >= npatient) {
            break;
        }
        sum_sz = sum_sz*2;
    }

    share* sum = circ->PutCONSGate(constant,sum_sz);

    // AND tree : comparison results of each patient are calculated
    for (int i = 0; i<npatient; i++) {
        uint32_t pos[nquery];
        for (int j=0; j<nquery; j++) {
            pos[j] = (i*nquery)+j;
        }
        share* part = circ->PutSubsetGate(s_out,pos,nquery);
        uint32_t nquery_t = nquery;
        while (1) {
            if (nquery_t == 1)
                break;
            uint32_t part_sz = nquery_t/2;
            uint32_t part_sz_1 = part_sz;
            if (nquery_t&0x1) {
                part_sz_1 += 1;
            }
            uint32_t pos1[part_sz_1];
            uint32_t pos2[part_sz_1];
            for (int j=0; j<part_sz; j++) {
                pos1[j] = j;
                pos2[j] = part_sz_1 + j;
            }
            if (nquery_t&0x1) {
                pos1[part_sz] = part_sz;
                pos2[part_sz] = part_sz;
            }


            share* part1 = circ->PutSubsetGate(part,pos1,part_sz_1);
            share* part2 = circ->PutSubsetGate(part,pos2,part_sz_1);;
            part = circ->PutANDGate(part1,part2);
            nquery_t = part_sz_1;;

        }

        // set result of AND of each patient to sum variable
        sum->set_wire_id(i,part->get_wire_id(0));
    }
    sum = circ->PutCombinerGate(sum);
    share* rem = circ->PutCONSGate(constant,32);
    while (1) {
        if (sum_sz == 1)
            break;
        uint32_t part_sz = sum_sz/2;
        uint32_t pos1[part_sz];
        uint32_t pos2[part_sz];
        for (int j=0; j<part_sz; j++) {
            pos1[j] = j;
            pos2[j] = part_sz+j;
        }
        share* part1 = circ->PutSubsetGate(sum,pos1,part_sz);
        share* part2 = circ->PutSubsetGate(sum,pos2,part_sz);
        sum = circ->PutADDGate(part1,part2);
        sum_sz = part_sz;
    }
    sum = circ->PutADDGate(sum,rem);
    share* tmp = circ->PutOUTGate(sum , ALL);

    party->ExecCircuit();
    o_time += party->GetTiming(P_ONLINE);
    os_time += party->GetTiming(P_SETUP);
    o_comm += party->GetSentData(P_ONLINE);
    s_comm += party->GetSentData(P_SETUP);

    output = tmp->get_clear_value<uint32_t>();

    cout<<"Offline time :\t" << os_time << endl;
    cout<<"Online time :\t" << o_time << endl;
    cout<<"Offline data :\t" << s_comm << endl;
    cout<<"Online data :\t" << o_comm << endl;

    cout<<"Result  "<<output<<endl;

    delete [] qu;
    delete [] vr;
    delete [] r;
    delete trees;
    delete party;
    return 0;
}
uint8_t getVal(uint8_t *array, uint32_t ind) {
    uint32_t index = ind/4;
    return (array[index]>>((ind%4)*2))&0x3;
}
