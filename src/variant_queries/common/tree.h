#ifndef __TREE_H_
#define __TREE_H_


#include "../../../abycore/ENCRYPTO_utils/typedefs.h"
#include <iostream>
#include <string>
using namespace std;
struct node {
    uint8_t* val;
    node* left;
    node* right;
};


class tree{
public:
    tree(uint16_t t,uint8_t s,uint32_t patient_count);
    tree(uint32_t patient_count);
    void setValues(string b,uint32_t patient_id);
    void setPatient(uint32_t patient_id);
    ~tree();
    void insert(uint64_t v);
    void destroy();
    node* getRoot();
    uint32_t getNodeCount();

private:
    node* root;
    node* current;
    void destroy(node* leaf);
    void createN(node** a);
    int createNode(node** a, uint32_t ind);
    void insert(uint64_t v,node* leaf,uint16_t t_depth);
    void setVal(node** a, uint8_t v);
    uint16_t DEPTH;
    uint8_t SIDE;
    string b;
    uint32_t P;
    uint32_t pid;
    uint32_t count;

};

#endif /* __TREE_H_ */