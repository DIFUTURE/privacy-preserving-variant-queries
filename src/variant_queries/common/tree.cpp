#include "tree.h"
tree::tree(uint16_t tree_depth,uint8_t role,uint32_t patient_count) {
    DEPTH = tree_depth;
    SIDE = role;
    P = (patient_count/4);
    if ((patient_count%4) > 0)
        P+=1;
    srand(time(NULL));
    root= new node;
    root->val=NULL;
    root->left=NULL;
    root->right=NULL;
    count =0;

}
tree::tree(uint32_t patient_count) {
    P = (patient_count/4);
    if ((patient_count%4) > 0)
        P+=1;
    cout<<P<<endl;
    root= new node;
    root->val=NULL;
    root->left=NULL;
    root->right=NULL;
    count = 0;

}
uint32_t tree::getNodeCount() {
    return count;
}
void tree::setValues(string bs, uint32_t patient_id) {
    ;
    node* a = root;
    b = bs;
    pid = patient_id;
    int ind = createNode(&(a->left),2);
    ind = createNode(&(a->right),ind);
    b.clear();
}
void tree::setPatient(uint32_t patient_id) {
    pid = patient_id;
}
void tree::setVal(node** a, uint8_t v) {
    uint32_t index = pid/4;
    uint8_t sub_index = pid%4;
    if ((((*a)->val[index]>>(sub_index*2))&0x3)==2)
        (*a)->val[index] = (*a)->val[index]^((v+2)<<(sub_index*2));
}
int tree::createNode(node** a, uint32_t ind) {
    int n = (b[ind]-48) + (b[ind+1]-48);
    if (n != 1) {
        if ((*a) == NULL) {
            count++;
            (*a) = new node;
            (*a)->val = new uint8_t[P];
            for (uint32_t i=0; i<P; i++)
                (*a)->val[i]=170;
            //      cout<<(*a)->val[0]<<endl;
            (*a)->left = NULL;
            (*a)->right = NULL;

        }
        setVal(&(*a),(b[ind]-48));
        ind = createNode(&((*a)->left),ind+2);
        ind = createNode(&((*a)->right),ind);
        return ind;
    }
    else {
        return ind+2;
    }
}
tree::~tree() {
    destroy();
}
void tree::destroy() {
    destroy(root);
}
void tree::destroy(node* leaf) {
    if (leaf != NULL) {
        destroy(leaf->right);
        destroy(leaf->left);
        delete[] leaf->val;
        delete leaf;
    }
}
void tree::insert(uint64_t v) {
    uint16_t t_depth = 0;
    insert(v,root,t_depth);
}
void tree::createN(node** a) {
    count++;
    (*a) = new node;
    (*a)->left=NULL;
    (*a)->right=NULL;
    (*a)->val = new uint8_t[P];
    for (uint32_t i=0; i<P; i++)
        (*a)->val[i]=170;
}
void tree::insert(uint64_t v,node* leaf,uint16_t t_depth) {
    if (t_depth < DEPTH) {
        uint8_t bit = (v>>t_depth)&0x1;
        t_depth+=1;
        if (bit == 0) {
            if (leaf->left == NULL) {
                createN(&(leaf->left));
            }
            setVal(&(leaf->left),(bit&SIDE));
            insert(v,leaf->left,t_depth);
        } else {
            if (leaf->right == NULL) {
                createN(&(leaf->right));
            }
            setVal(&(leaf->right),(bit&SIDE));
            insert(v,leaf->right,t_depth);
        }
    }
}
node* tree::getRoot() {
    return root;
}
