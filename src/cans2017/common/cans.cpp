#include "cans.h"

int32_t test_cans(e_role role, char* address, uint16_t port, seclvl seclvl,
                  uint32_t bitlen, uint32_t nthreads, e_mt_gen_alg mt_alg,
                  e_sharing sharing,uint32_t nvariant,uint32_t nquery) {


    ABYParty* party = new ABYParty(role, address, port, seclvl, bitlen, nthreads,
                                   mt_alg);

    double o_time  = 0;
    double os_time  = 0;

    vector<Sharing*>& sharings = party->GetSharings();
    BooleanCircuit* circ = (BooleanCircuit*) sharings[sharing]->GetCircuitBuildRoutine();

    share *s_query, *s_variant, *s_out;

    uint64_t query[nquery], *variant;
    variant = new uint64_t[nvariant];
    srand(time(NULL));

    // generate random variants
    for (int j=0; j<nvariant; j++) {
        variant[j] = rand();
    }

    // we set query varaints to first nquery variant values in shared form.
    // we will get result 1
    // please assign random values for result 0 or 1
    for (int i=0; i<nquery; i++)
        query[i]=variant[i];

    s_variant = circ->PutSharedSIMDINGate(nvariant, variant, bitlen);
    s_query = circ->PutSharedSIMDINGate(nquery, query, bitlen);


    uint32_t total = nquery*nvariant;
    uint32_t *positions = new uint32_t[total];

    // repeat queries as many the number of patients
    int ind = -1;
    for (int i=0; i<total; i++) {
        if (i%nvariant == 0)
            ind=ind+1;
        positions[i]=ind;
    }
    s_query  = circ->PutSubsetGate(s_query,positions,total);

    // repeat variants as many the number of query variants
    for (int i=0; i<total; i++) {
        positions[i]=i%nvariant;
    }
    s_variant  = circ->PutSubsetGate(s_variant,positions,total);
    // v1 v2 v3 v4 v1 v2 v3 v4
    // q1 q1 q1 q1 q2 q2 q2 q2

    delete [] positions;

    // compare query variants with variants in VCF
    s_out = circ->PutEQGate(s_query, s_variant);

    delete [] variant;


    // OR Tree : check if any of variants in VCF file matches with a query variant
    uint64_t constant = 0;
    share* s_and = circ->PutSIMDCONSGate(1,constant,nquery);
    positions = new uint32_t[nvariant];
    for (int i=0; i<nquery; i++) {
        for (int j=0; j<nvariant; j++) {
            positions[j]=j+(i*nvariant);
        }
        share* part = circ->PutSubsetGate(s_out,positions,nvariant);
        uint32_t nvariant_t = nvariant;

        while (1) {
            uint32_t part_sz = nvariant_t/2;
            uint32_t part_sz_1 = part_sz;
            if (nvariant_t&0x1) {
                part_sz_1 +=1;
            }
            uint32_t *p1 = new uint32_t[part_sz_1];
            uint32_t *p2 = new uint32_t[part_sz_1];
            for (int j=0; j<part_sz; j++) {
                p1[j] = j;
                p2[j] = part_sz_1+j;
            }
            if (nvariant_t&0x1) {
                p1[part_sz] = part_sz;
                p2[part_sz] = part_sz;
            }


            share* part1 = circ->PutSubsetGate(part,p1,part_sz_1);
            share* part2 = circ->PutSubsetGate(part,p2,part_sz_1);
            part = circ->PutORGate(part1,part2);
            nvariant_t = part_sz_1;

            delete [] p1;
            delete [] p2;
            if (nvariant_t == 1)
                break;

        }
        s_and->set_wire_id(i,part->get_wire_id(0));
    }

    delete [] positions;
    // AND Tree : check all query variants match with any variant in VCF file
    s_and = circ->PutCombinerGate(s_and);
    uint32_t nquery_t = nquery;
    while (1) {
        if (nquery_t == 1)
            break;
        uint32_t part_sz = nquery_t/2;
        uint32_t part_sz_1 = part_sz;
        if (nquery_t&0x1) {
            part_sz_1 +=1;
        }
        uint32_t pos1[part_sz_1];
        uint32_t pos2[part_sz_1];
        for (int j=0; j<part_sz; j++) {
            pos1[j] = j;
            pos2[j] = part_sz_1+j;
        }
        if (nquery_t&0x1) {
            pos1[part_sz] = part_sz;
            pos2[part_sz] = part_sz;
        }


        share* part1 = circ->PutSubsetGate(s_and,pos1,part_sz_1);
        share* part2 = circ->PutSubsetGate(s_and,pos2,part_sz_1);
        s_and = circ->PutANDGate(part1,part2);
        nquery_t = part_sz_1;

    }


    share* tmp = circ->PutOUTGate(s_and , ALL);
    party->ExecCircuit();
    o_time += party->GetTiming(P_ONLINE);
    os_time += party->GetTiming(P_SETUP);

    uint32_t output = tmp->get_clear_value<uint32_t>();

    cout<<"Result : "<<output<<endl;
    party->Reset();

    cout<<"Offline time :\t" << os_time << endl;
    cout<<"Online time :\t" << o_time << endl;

    delete party;
    return 0;
}

