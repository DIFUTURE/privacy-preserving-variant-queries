	//Utility libs
	#include "../../abycore/ENCRYPTO_utils/crypto/crypto.h"
	#include "../../abycore/ENCRYPTO_utils/parse_options.h"
	//ABY Party class
	#include "../../abycore/aby/abyparty.h"

	#include "common/cans.h"

	int32_t read_test_options(int32_t* argcp, char*** argvp, e_role* role,
			uint32_t* bitlen,  uint32_t* secparam, string* address,
			uint16_t* port, uint32_t* nvariant, uint32_t* nquery, uint32_t* nlen) {

		uint32_t int_role = 0, int_port = 0;
		bool useffc = false;

		parsing_ctx options[] =
				{{ (void*) &int_role, T_NUM, "r", "Role: 0/1", true, false }, 
				{(void*) nvariant, T_NUM, "v", "Number of Variants", true, false },
				{(void*) nquery, T_NUM, "q", "Number of Queries", true, false },
				{(void*) nlen, T_NUM, "l", "The Bit Lenght of Variant", true, false },
				{(void*) bitlen, T_NUM, "b", "Bit-length, default 32", false,false }, 
				{(void*) secparam, T_NUM, "s","Symmetric Security Bits, default: 128", false, false }, 
				{(void*) address, T_STR, "a","IP-address, default: localhost", false, false }, 
				{(void*) &int_port, T_NUM, "p", "Port, default: 7766", false,false } };

		if (!parse_options(argcp, argvp, options,
				sizeof(options) / sizeof(parsing_ctx))) {
			print_usage(*argvp[0], options, sizeof(options) / sizeof(parsing_ctx));
			cout << "Exiting" << endl;
			exit(0);
		}

		assert(int_role < 2);
		*role = (e_role) int_role;

		if (int_port != 0) {
			assert(int_port < 1 << (sizeof(uint16_t) * 8));
			*port = (uint16_t) int_port;
		}

		return 1;
	}

	int main(int argc, char** argv) {

		e_role role;
		uint32_t secparam = 128, nthreads =1; 
		uint16_t port = 7766;
		string address = "127.0.0.1";
		uint32_t nvariant = 100000;
		uint32_t nquery = 5;
		uint32_t nlen = 48;
		e_mt_gen_alg mt_alg = MT_OT;

		read_test_options(&argc, &argv, &role, &nlen, &secparam, &address,
				&port, &nvariant,&nquery,&nlen);

		seclvl seclvl = get_sec_lvl(secparam);

		test_cans(role, (char*) address.c_str(), port, seclvl, nlen,
				nthreads, mt_alg, S_BOOL,nvariant,nquery);

		return 0;
	}

