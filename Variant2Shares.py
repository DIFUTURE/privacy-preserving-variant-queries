#!/usr/bin/python
# -*- coding: utf-8 -*-

import hashlib
import math
import random
import sys
import argparse

chr_size = {'chr1': 0, 'chr2': 248956422, 'chr3': 491149951, 'chr4': 689445510, 'chr5': 879660065, 'chr6': 1061198324, 'chr7': 1232004303, 'chr8': 1391350276, 'chr9': 1536488912, 'chr10': 1674883629, 'chr11': 1808681051, 'chr12': 1943767673, 'chr13': 2077042982, 'chr14': 2191407310, 'chr15': 2298451028, 'chr16': 2400442217, 'chr17': 2490780562, 'chr18': 2574038003, 'chr19': 2654411288, 'chr20': 2713028904, 'chr21': 2777473071, 'chr22': 2824183054, 'chrX': 2875001522, 'chrY': 3031042417, 'chrM': 3088269832}

if __name__ == '__main__':
    parser = \
        argparse.ArgumentParser(description='Converts a given variant to two boolean shares'
                                )
    parser.add_argument('-c', action='store', dest='chr',
                        required=True, help='CHROM')
    parser.add_argument(
        '-p',
        action='store',
        dest='pos',
        required=True,
        help='POS',
        )
    parser.add_argument(
        '-a',
        action='store',
        dest='alt',
        required=True,
        help='ALT',
        )
    parser.add_argument(
        '-r',
        action='store',
        dest='ref',
        required=True,
        help='REF',
        )
    results = parser.parse_args()

    m = hashlib.md5()
    m.update((results.chr+results.pos+results.ref+results.alt).encode())
    value = int(m.hexdigest()[:4], 16)
    upos = chr_size[results.chr] + int(results.pos)
    pos_value = (value << 32) + upos
    rand_value = random.randint(0,281474976710656)
    print(pos_value," = ",pos_value^rand_value,"^",rand_value)
